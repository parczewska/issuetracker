package pl.pa.issue.tracker.issue.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.pa.issue.tracker.issue.api.IssueApi;
import pl.pa.issue.tracker.issue.error.InvalidIssueDataException;
import pl.pa.issue.tracker.issue.model.IssueDTO;
import pl.pa.issue.tracker.issue.model.PriorityType;

import java.util.Collection;
import java.util.List;

public class IssueApiTest {

    private static IssueRepository issueRepository = new IssueRepositoryInMemory();
    private static IssueService issueService = new IssueService(issueRepository);
    private static IssueApi issueApi = new IssueApi(issueService);


    @BeforeAll
    static void init() {
        IssueDTO issueDTO = IssueDTO.builder().title("zadanie").description("wykonaj zadanie")
                .priority(PriorityType.MEDIUM.name()).username("mkowalski").build();
        issueApi.create(issueDTO);
    }

    @Test
    void shouldFindAllIssues() {
        //given
        Collection<IssueDTO> issueDTO = issueApi.issues();
        //then
        Assertions.assertNotNull(issueDTO);
    }

    @Test
    void shouldCreateIssue() {
        //given
        IssueDTO issueDTO = IssueDTO.builder().title("zadanie domowe").description("wykonaj zadanie w domu")
                .priority(PriorityType.LOW.name()).username("mnosowska").build();
        issueApi.create(issueDTO);
        //when
        Collection<IssueDTO> result = issueApi.show("mnosowska");
        //then
        Assertions.assertEquals(1, result.size());
    }

    @Test
    void shouldShowIssueIfAccountExist() {
        //given
        IssueDTO issueDTO = IssueDTO.builder().title("zadanie2").description("wykonaj zadanie2")
                .priority(PriorityType.LOW.name()).username("okwiatek").build();
        issueApi.create(issueDTO);
        //when
        Collection<IssueDTO> result = issueApi.show("okwiatek");
        //then
        Assertions.assertEquals(1, result.size());
    }

    @Test
    void shouldNotShowIssueIfAccountNotExist() {
        //then
        Assertions.assertThrows(InvalidIssueDataException.class, () -> {
            issueApi.show("bbatory");
        });
    }

    @Test
    void shouldDeleteIfIssueExist() {
        //then
        Assertions.assertDoesNotThrow(()->{issueApi.delete(0);});
    }

    @Test
    void shouldNotDeleteIfIssueNotExist() {
        //then
        Assertions.assertThrows(InvalidIssueDataException.class, () -> {
            issueApi.delete(99);
        });
    }
}
