package pl.pa.issue.tracker.issue.domain;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import pl.pa.issue.tracker.issue.domain.IssueRepository;
import pl.pa.issue.tracker.issue.model.Issue;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class IssueRepositoryInMemory implements IssueRepository {

    private Set<Issue> issues = new HashSet<>();

    @Override
    public List<Issue> findAllByUsername(String username) {

        return issues.stream().filter(e -> e.getUsername().equals(username)).collect(Collectors.toList());
    }

    @Override
    public List<Issue> findAll() {
        return issues.stream().collect(Collectors.toUnmodifiableList());
    }

    @Override
    public List<Issue> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<Issue> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<Issue> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Issue entity) {
        issues.remove(entity);

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends Issue> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends Issue> S save(S entity) {
        if (issues.add(entity)) {
            return entity;
        }
        return null;
    }

    @Override
    public <S extends Issue> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Issue> findById(Long aLong) {
        return issues.stream().filter(e -> e.getId() == aLong).findFirst();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends Issue> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends Issue> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<Issue> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<Long> longs) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public Issue getOne(Long aLong) {
        return null;
    }

    @Override
    public Issue getById(Long aLong) {
        return null;
    }

    @Override
    public <S extends Issue> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends Issue> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends Issue> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends Issue> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends Issue> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends Issue> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends Issue, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }
}
