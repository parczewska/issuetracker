package pl.pa.issue.tracker.account.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.pa.issue.tracker.account.api.AccountApi;
import pl.pa.issue.tracker.account.error.InvalidAccountDataException;
import pl.pa.issue.tracker.role.domain.RoleRepository;
import pl.pa.issue.tracker.role.domain.RoleRepositoryInMemory;

public class AccountApiTest {

    private static AccountRepository accountRepository = new AccountRepositoryInMemory();
    private static RoleRepository roleRepository = new RoleRepositoryInMemory();
    private static AccountService accountService = new AccountService(accountRepository, roleRepository);
    private static AccountApi accountApi = new AccountApi(accountService);

    @BeforeAll
    static void init() {
        AccountDTO accountDTO = AccountDTO.builder().username("anowak").name("Anna").lastname("Nowak").build();
        accountApi.add(accountDTO);
    }

    @Test
    void shouldFindUserByUsername() {
        //then
        Assertions.assertDoesNotThrow(() -> {
            accountApi.findAccountByUsername("anowak");
        });
    }

    @Test
    void shouldNotFindUserByUserNameIfUsernameNotExist() {
        //then
        Assertions.assertThrows(InvalidAccountDataException.class, () -> {
            AccountDTO result = accountApi.findAccountByUsername("akowalska");
        });
    }

    @Test
    void shouldAddAccount() {
        //given
        AccountDTO accountDTO = AccountDTO.builder().username("Akowalski").name("Adam").lastname("Kowalski").build();
        accountApi.add(accountDTO);
        //then
        Assertions.assertDoesNotThrow(() -> {
            accountApi.findAccountByUsername("Akowalski");
        });
    }

    @Test
    void shouldDeleteAccount() {
        //given
        accountApi.delete("anowak");
        //then
        Assertions.assertThrows(InvalidAccountDataException.class, () -> {
            accountApi.findAccountByUsername("anowak");
        });
    }

    @Test
    void shouldNotDeleteAccountIfNotExist() {
        Assertions.assertThrows(InvalidAccountDataException.class, () -> {
            accountApi.delete("imienazwisko");
        });
    }
}
