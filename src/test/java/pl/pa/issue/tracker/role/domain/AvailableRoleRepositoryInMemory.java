package pl.pa.issue.tracker.role.domain;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import pl.pa.issue.tracker.role.domain.AvailableRoleRepository;
import pl.pa.issue.tracker.role.model.AvailableRole;

import java.util.*;
import java.util.function.Function;

public class AvailableRoleRepositoryInMemory implements AvailableRoleRepository {

    List<AvailableRole> availableRoles = List.of(new AvailableRole("admin"), new AvailableRole("user"), new AvailableRole("guest"));

    @Override
    public AvailableRole findByRole(String role) {
        return availableRoles.stream().filter(e -> e.getRole().equals(role)).findFirst().get();
    }

    @Override
    public List<AvailableRole> findAll() {
        return new ArrayList<>(availableRoles);
    }

    @Override
    public List<AvailableRole> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<AvailableRole> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<AvailableRole> findAllById(Iterable<String> strings) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(String s) {

    }

    @Override
    public void delete(AvailableRole entity) {

    }

    @Override
    public void deleteAllById(Iterable<? extends String> strings) {

    }

    @Override
    public void deleteAll(Iterable<? extends AvailableRole> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends AvailableRole> S save(S entity) {
        return null;
    }

    @Override
    public <S extends AvailableRole> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<AvailableRole> findById(String s) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(String s) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends AvailableRole> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends AvailableRole> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<AvailableRole> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<String> strings) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public AvailableRole getOne(String s) {
        return null;
    }

    @Override
    public AvailableRole getById(String s) {
        return null;
    }

    @Override
    public <S extends AvailableRole> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends AvailableRole> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends AvailableRole> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends AvailableRole> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends AvailableRole> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends AvailableRole> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends AvailableRole, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }
}
