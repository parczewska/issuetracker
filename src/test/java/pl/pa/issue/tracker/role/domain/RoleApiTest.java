package pl.pa.issue.tracker.role.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.pa.issue.tracker.account.api.AccountApi;

import pl.pa.issue.tracker.account.domain.AccountDTO;
import pl.pa.issue.tracker.account.domain.AccountRepository;
import pl.pa.issue.tracker.account.domain.AccountRepositoryInMemory;
import pl.pa.issue.tracker.account.domain.AccountService;
import pl.pa.issue.tracker.role.api.RoleApi;
import pl.pa.issue.tracker.role.error.InvalidRoleDataException;
import pl.pa.issue.tracker.role.model.RoleDTO;

public class RoleApiTest {

    private static AvailableRoleRepository availableRoleRepository = new AvailableRoleRepositoryInMemory();
    private static RoleRepository roleRepository = new RoleRepositoryInMemory();
    private static AccountRepository accountRepository = new AccountRepositoryInMemory();
    private static AvailableRoleService availableRoleService = new AvailableRoleService(availableRoleRepository);
    private static RoleService roleService = new RoleService(roleRepository, availableRoleService);
    private static RoleApi roleApi = new RoleApi(roleService);
    private static AccountService accountService = new AccountService(accountRepository, roleRepository);
    private static AccountApi accountApi = new AccountApi(accountService);


    @BeforeAll
    static void init() {
        AccountDTO accountDTO = AccountDTO.builder().username("anowak").name("Anna").lastname("Nowak").build();
        accountApi.add(accountDTO);
    }

    @Test
    void shouldShowRoleIfAccountExist() {
        //given
        RoleDTO roleDTO = RoleDTO.builder().username("anowak").role("admin").build();
        roleApi.assign(roleDTO);
        //when
        RoleDTO result = roleApi.show("anowak");
        //then
        Assertions.assertEquals("admin", result.getRole());
    }

    @Test
    void shouldNotShowRoleIfAccountNotExist() {
        //then
        Assertions.assertThrows(InvalidRoleDataException.class, () -> {
            RoleDTO result = roleApi.show("jakislogin");
        });
    }

    @Test
    void shouldDeleteRoleIfAccountExist() {
        //given
        roleApi.delete("anowak");
        //then
        RoleDTO result = roleApi.show("anowak");
        Assertions.assertEquals("", result.getRole());
    }

    @Test
    void shouldNotDeleteRoleIfAccountNotExist() {
        Assertions.assertThrows(InvalidRoleDataException.class, () -> {
            roleApi.delete("login");
        });
    }

    @Test
    void shouldAssignRoleIfAccountExist() {
        //given
        RoleDTO roleDTO = RoleDTO.builder().username("anowak").role("admin").build();
        RoleDTO result = roleApi.assign(roleDTO);
        //then
        Assertions.assertEquals(roleDTO, result);
    }

    @Test
    void shouldNotAssignRoleIfAccountNotExist() {
        //given
        RoleDTO roleDTO = RoleDTO.builder().username("knowakowska").role("admin").build();
        //then
        Assertions.assertThrows(InvalidRoleDataException.class, () -> {
            RoleDTO result = roleApi.assign(roleDTO);
        });
    }

    @Test
    void shouldNotAssignRoleIfRoleNotExist() {
        //given
        RoleDTO roleDTO = RoleDTO.builder().username("umichalik").role("jakasRola").build();
        //then
        Assertions.assertThrows(InvalidRoleDataException.class, () -> {
            RoleDTO result = roleApi.assign(roleDTO);
        });
    }


}
