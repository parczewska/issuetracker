package pl.pa.issue.tracker.assign.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.pa.issue.tracker.account.api.AccountApi;
import pl.pa.issue.tracker.account.domain.AccountDTO;
import pl.pa.issue.tracker.account.domain.AccountRepository;
import pl.pa.issue.tracker.account.domain.AccountRepositoryInMemory;
import pl.pa.issue.tracker.account.domain.AccountService;
import pl.pa.issue.tracker.assign.api.AssignApi;
import pl.pa.issue.tracker.assign.error.InvalidAssignDataException;
import pl.pa.issue.tracker.assign.model.AssignDTO;
import pl.pa.issue.tracker.issue.api.IssueApi;
import pl.pa.issue.tracker.issue.domain.IssueRepository;
import pl.pa.issue.tracker.issue.domain.IssueRepositoryInMemory;
import pl.pa.issue.tracker.issue.domain.IssueService;
import pl.pa.issue.tracker.issue.model.Issue;
import pl.pa.issue.tracker.issue.model.IssueDTO;
import pl.pa.issue.tracker.issue.model.PriorityType;
import pl.pa.issue.tracker.role.domain.RoleRepository;
import pl.pa.issue.tracker.role.domain.RoleRepositoryInMemory;

import java.util.Optional;

public class AssignApiTest {


    private static IssueRepository issueRepository = new IssueRepositoryInMemory();
    private static RoleRepository roleRepository = new RoleRepositoryInMemory();
    private static IssueService issueService = new IssueService(issueRepository);
    private static IssueApi issueApi = new IssueApi(issueService);
    private static AccountRepository accountRepository = new AccountRepositoryInMemory();
    private static AccountService accountService = new AccountService(accountRepository, roleRepository);
    private static AccountApi accountApi = new AccountApi(accountService);
    private static AssignService assignService = new AssignService(issueRepository);
    private static AssignApi assignApi = new AssignApi(assignService);


    @BeforeAll
    static void init() {
        AccountDTO accountDTO = AccountDTO.builder().username("anowak").username("anna").lastname("kowalska").build();
        accountApi.add(accountDTO);

        IssueDTO issueDTO = IssueDTO.builder().title("zadanie nr 1").description("wykonaj zadanie nr 1")
                .priority(PriorityType.HIGH.name()).username("anowak").build();
        issueApi.create(issueDTO);
    }

    @Test
    void shouldAssignIfIssueExist() {
        //given
        AssignDTO expected = AssignDTO.builder().id(0).username("anowak").build();
        //then
        AssignDTO result = assignApi.assign(expected);
        //when
        Assertions.assertEquals(expected, result);
    }

    @Test
    void shouldNotAssignIfIssueNotExist() {
        //given
        AssignDTO assignDTO = AssignDTO.builder().id(99).username("hkowalik").build();
        //then
        Assertions.assertThrows(InvalidAssignDataException.class, () -> {
            assignApi.assign(assignDTO);
        });
    }

    @Test
    void shouldDeleteAssignIfIssueExist() {
        //given
        Optional<Issue> result = issueRepository.findById(0L);
        Issue issue = result.get();
        //when
        Assertions.assertNotEquals("", issue.getUsername());
        assignApi.unassignIssue(0);
        //then
        Assertions.assertEquals("", issue.getUsername());
    }

    @Test
    void shouldNotDeleteAssignIfIssueNotExist() {
        Assertions.assertThrows(InvalidAssignDataException.class, () -> {
            assignApi.unassignIssue(55);
        });
    }
}
