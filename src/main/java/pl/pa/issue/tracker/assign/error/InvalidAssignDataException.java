package pl.pa.issue.tracker.assign.error;

public class InvalidAssignDataException extends RuntimeException {

    public InvalidAssignDataException(String message) {
        super(message);
    }
}
