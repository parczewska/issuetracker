package pl.pa.issue.tracker.assign.api;

import org.springframework.web.bind.annotation.*;
import pl.pa.issue.tracker.assign.model.AssignDTO;
import pl.pa.issue.tracker.assign.domain.AssignService;

@RestController
public class AssignApi {

    private AssignService assignService;

    public AssignApi(AssignService assignService) {
        this.assignService = assignService;
    }

    @RequestMapping(value = "/assign", method = RequestMethod.POST)
    public AssignDTO assign(@RequestBody AssignDTO assignDTO) {
        return assignService.assignIssue(assignDTO);
    }

    @RequestMapping(value = "/assign/{id}", method = RequestMethod.DELETE)
    public void unassignIssue(@PathVariable long id) {
        assignService.unassignIssue(id);
    }
}
