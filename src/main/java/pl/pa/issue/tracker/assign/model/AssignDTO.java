package pl.pa.issue.tracker.assign.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class AssignDTO {

    @JsonProperty
    private long id;

    @JsonProperty
    private String username;

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }
}
