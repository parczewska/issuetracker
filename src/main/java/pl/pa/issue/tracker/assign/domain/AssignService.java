package pl.pa.issue.tracker.assign.domain;

import org.springframework.stereotype.Service;
import pl.pa.issue.tracker.assign.error.InvalidAssignDataException;
import pl.pa.issue.tracker.assign.model.AssignDTO;
import pl.pa.issue.tracker.issue.domain.IssueRepository;
import pl.pa.issue.tracker.issue.model.Issue;

import java.util.Optional;

@Service
public class AssignService {

    private IssueRepository issueRepository;


    public AssignService(IssueRepository issueRepository) {
        this.issueRepository = issueRepository;
    }

    public AssignDTO assignIssue(AssignDTO assignDTO) {

        Optional<Issue> result = issueRepository.findById(assignDTO.getId());
        if (result.isPresent()) {
            Issue issue = result.get();

            if (issue.getUsername() == null || issue.getUsername().equals("")) {
                issue.setUsername(assignDTO.getUsername());
                issueRepository.save(issue);
                return assignDTO;
            }
            throw new InvalidAssignDataException("Zadanie jest już przypisane do użytkownika.");
        }
        throw new InvalidAssignDataException("Nie ma zadania o podanym id.");

    }

    public void unassignIssue(long id) {
        Optional<Issue> result = issueRepository.findById(id);

        if (result.isPresent()) {
            Issue issue = result.get();
            issue.setUsername("");
            issueRepository.save(issue);
        } else throw new InvalidAssignDataException("Nie ma zadania o podanym id " + id + ".");
    }
}
