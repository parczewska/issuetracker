package pl.pa.issue.tracker.issue.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class IssueDTO {

    @JsonProperty
    private String title;
    @JsonProperty
    private String description;
    @JsonProperty
    private String priority;
    @JsonProperty
    private String username;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPriority() {
        return priority;
    }

    public String getUsername() {
        return username;
    }
}
