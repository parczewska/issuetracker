package pl.pa.issue.tracker.issue.model;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class IssuesMapping {

    public static Collection<IssueDTO> map(Collection<Issue> issues) {

        List<IssueDTO> issuesDTO = new LinkedList<>();

        for (Issue issue : issues) {
            IssueDTO issueDTO = IssueDTO.builder()
                    .title(issue.getTitle())
                    .description(issue.getDescription())
                    .priority(issue.getPriority())
                    .username(issue.getUsername()).build();

            issuesDTO.add((issueDTO));
        }
        return issuesDTO;
    }
}
