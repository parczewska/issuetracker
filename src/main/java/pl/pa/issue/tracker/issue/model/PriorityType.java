package pl.pa.issue.tracker.issue.model;

public enum PriorityType {

    HIGH,
    MEDIUM,
    LOW
}
