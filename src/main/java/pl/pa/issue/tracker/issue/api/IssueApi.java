package pl.pa.issue.tracker.issue.api;

import org.springframework.web.bind.annotation.*;
import pl.pa.issue.tracker.issue.model.IssueDTO;
import pl.pa.issue.tracker.issue.domain.IssueService;

import java.util.Collection;
import java.util.List;

@RestController
public class IssueApi {

    private IssueService issueService;

    public IssueApi(IssueService issueService) {
        this.issueService = issueService;
    }

    @RequestMapping(value = "/issues", method = RequestMethod.GET)
    public Collection<IssueDTO> issues() {
        return issueService.issues();
    }

    @RequestMapping(value = "/issues", method = RequestMethod.POST)
    public IssueDTO create(@RequestBody IssueDTO issueDTO) {
        return issueService.createIssue(issueDTO);
    }

    @RequestMapping(value = "/issues/{username}", method = RequestMethod.GET)
    public Collection<IssueDTO> show(@PathVariable String username) {
        return issueService.showIssue(username);
    }

    @RequestMapping(value = "/issues/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable long id) {
        issueService.deleteIssue(id);
    }
}

