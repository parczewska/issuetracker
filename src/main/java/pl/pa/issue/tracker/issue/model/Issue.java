package pl.pa.issue.tracker.issue.model;

import javax.persistence.*;

@Entity
@Table(name = "issues")
public class Issue {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;
    @Enumerated(EnumType.STRING)
    @Column(name = "priority")
    private PriorityType priority;
    @Column(name = "username")
    private String username;

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPriority() {
        return priority.name();
    }

    public String getUsername() {
        return username;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPriority(String priority) {
        this.priority = PriorityType.valueOf(priority);
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
