package pl.pa.issue.tracker.issue.error;

public class InvalidIssueDataException extends RuntimeException{

    public InvalidIssueDataException(String message) {
        super(message);
    }
}
