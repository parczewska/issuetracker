package pl.pa.issue.tracker.issue.domain;


import org.springframework.stereotype.Service;
import pl.pa.issue.tracker.issue.error.InvalidIssueDataException;
import pl.pa.issue.tracker.issue.model.Issue;
import pl.pa.issue.tracker.issue.model.IssueDTO;
import pl.pa.issue.tracker.issue.model.IssuesMapping;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class IssueService {

    private IssueRepository issueRepository;

    public IssueService(IssueRepository issueRepository) {
        this.issueRepository = issueRepository;
    }

    public Collection<IssueDTO> issues() {
        List<Issue> issuses = issueRepository.findAll();
        return IssuesMapping.map(issuses);
    }

    public IssueDTO createIssue(IssueDTO issueDTO) {

        Issue issue = new Issue();
        issue.setTitle(issueDTO.getTitle());
        issue.setDescription(issueDTO.getDescription());
        issue.setPriority(issueDTO.getPriority());
        issue.setUsername(issueDTO.getUsername());
        issueRepository.save(issue);
        return issueDTO;
    }

    public Collection<IssueDTO> showIssue(String username) {

        Collection<Issue> result = issueRepository.findAllByUsername(username);
        if (!result.isEmpty()) {
            return IssuesMapping.map(result);
        }
        throw new InvalidIssueDataException("Nie istniej użytkownik, nie można wyswietlić przypisanych mu zadań.");
    }

    public void deleteIssue(long id) {

        Optional<Issue> result = issueRepository.findById(id);
        if (result.isPresent()) {
            issueRepository.delete(result.get());
        } else throw new InvalidIssueDataException("Użytkownik nie posiada przypisanego zadania.");
    }
}
