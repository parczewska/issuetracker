package pl.pa.issue.tracker.account.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account {

    @Id
    @Column(name = "username")
    private String username;
    @Column(name = "name")
    private String name;
    @Column(name = "last_name")
    private String lastname;

    String getUsername() {
        return username;
    }

    String getName() {
        return name;
    }

    String getLastname() {
        return lastname;
    }

    void setUsername(String username) {
        this.username = username;
    }

    void setName(String name) {
        this.name = name;
    }

    void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
