package pl.pa.issue.tracker.account.error;

public class InvalidAccountDataException extends RuntimeException{

    public InvalidAccountDataException(String message) {
        super(message);
    }
}
