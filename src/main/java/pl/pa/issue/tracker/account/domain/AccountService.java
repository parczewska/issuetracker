package pl.pa.issue.tracker.account.domain;

import org.springframework.stereotype.Service;
import pl.pa.issue.tracker.account.error.InvalidAccountDataException;
import pl.pa.issue.tracker.role.domain.RoleRepository;
import pl.pa.issue.tracker.role.model.Role;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    private AccountRepository accountRepository;
    private RoleRepository roleRepository;

    public AccountService(AccountRepository accountRepository, RoleRepository roleRepository) {
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
    }

    public AccountDTO findByUsername(String username) {

        Optional<Account> result = accountRepository.findByUsername(username);

        if (result.isPresent()) {
            return AccountMapper.map(result.get());
        }
        throw new InvalidAccountDataException("Nie istnieje konto o podanym loginie (username).");
    }

    public AccountDTO addAccount(AccountDTO accountDTO) {

        Account account = AccountMapper.map(accountDTO);
        accountRepository.save(account);
        Role role = new Role();
        role.setUsername(account.getUsername());
        role.setRole("");
        roleRepository.save(role);
        return accountDTO;
    }

    public void deleteAccount(String username) {

        Optional<Account> result = accountRepository.findByUsername(username);

        if (result.isPresent()) {
            accountRepository.delete(result.get());
        } else
            throw new InvalidAccountDataException("Nie istnieje konto o podanym loginie (username).");
    }
}
