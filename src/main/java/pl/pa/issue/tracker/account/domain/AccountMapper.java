package pl.pa.issue.tracker.account.domain;

class AccountMapper {

    static AccountDTO map(Account account) {

        AccountDTO accountDTO = AccountDTO.builder().username(account.getUsername()).
                name(account.getName()).lastname(account.getLastname()).build();

        return accountDTO;
    }

    static Account map(AccountDTO accountDTO) {

        Account account = new Account();
        account.setUsername(accountDTO.getUsername());
        account.setName(accountDTO.getName());
        account.setLastname(accountDTO.getLastname());

        return account;
    }
}
