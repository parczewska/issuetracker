package pl.pa.issue.tracker.account.api;

import org.springframework.web.bind.annotation.*;
import pl.pa.issue.tracker.account.domain.AccountDTO;
import pl.pa.issue.tracker.account.domain.AccountService;

import java.util.Collection;

@RestController
public class AccountApi {

    private AccountService accountService;

    public AccountApi(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(value = "account/{username}", method = RequestMethod.GET)
    public AccountDTO findAccountByUsername(@PathVariable String username) {
        return accountService.findByUsername(username);
    }

    @RequestMapping(value = "/account", method = RequestMethod.POST)
    public AccountDTO add(@RequestBody AccountDTO accountDTO) {
        return accountService.addAccount(accountDTO);
    }

    @RequestMapping(value = "/account/{username}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String username) {
        accountService.deleteAccount(username);

    }
}
