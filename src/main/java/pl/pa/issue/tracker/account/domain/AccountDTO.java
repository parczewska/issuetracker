package pl.pa.issue.tracker.account.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class AccountDTO {

    @JsonProperty
    private String username;
    @JsonProperty
    private String name;
    @JsonProperty
    private String lastname;

    String getUsername() {
        return username;
    }

    String getName() {
        return name;
    }

    String getLastname() {
        return lastname;
    }
}
