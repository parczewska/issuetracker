package pl.pa.issue.tracker.account.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.pa.issue.tracker.issue.model.Issue;

import java.util.Collection;
import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, String> {

    Optional<Account> findByUsername(String username);
}
