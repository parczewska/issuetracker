package pl.pa.issue.tracker.role.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pa.issue.tracker.role.model.AvailableRole;

@Repository
public interface AvailableRoleRepository extends JpaRepository<AvailableRole, String> {

    AvailableRole findByRole(String role);

}
