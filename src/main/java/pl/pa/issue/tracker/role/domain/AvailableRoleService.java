package pl.pa.issue.tracker.role.domain;

import org.springframework.stereotype.Service;
import pl.pa.issue.tracker.role.model.AvailableRole;

@Service
public class AvailableRoleService {

    private AvailableRoleRepository availableRoleRepository;

    public AvailableRoleService(AvailableRoleRepository availableRoleRepository) {
        this.availableRoleRepository = availableRoleRepository;
    }

    public boolean isExist(String role) {

        AvailableRole result = availableRoleRepository.findByRole(role);
        return (result != null);
    }
}
