package pl.pa.issue.tracker.role.error;

public class InvalidRoleDataException extends RuntimeException {
    public InvalidRoleDataException(String message) {
        super(message);
    }
}
