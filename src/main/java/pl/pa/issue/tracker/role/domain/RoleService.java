package pl.pa.issue.tracker.role.domain;

import org.springframework.stereotype.Service;
import pl.pa.issue.tracker.role.error.InvalidRoleDataException;
import pl.pa.issue.tracker.role.model.Role;
import pl.pa.issue.tracker.role.model.RoleDTO;

import java.util.Optional;

@Service
public class RoleService {

    private RoleRepository roleRepository;
    private AvailableRoleService availableRoleService;

    public RoleService(RoleRepository roleRepository, AvailableRoleService availableRoleService) {

        this.roleRepository = roleRepository;
        this.availableRoleService = availableRoleService;
    }

    public RoleDTO assignRole(RoleDTO roleDTO) {

        Optional<Role> result = roleRepository.findByUsername(roleDTO.getUsername());

        if (result.isPresent()) {

            if (availableRoleService.isExist(roleDTO.getRole())) {

                Role role = result.get();
                role.setRole(roleDTO.getRole());
                roleRepository.save(role);
                return roleDTO;
            }
            throw new InvalidRoleDataException("Nie można nadać roli " + roleDTO.getRole() + ".");
        }
        throw new InvalidRoleDataException("Nie istnieje użytkownik dla którego chcesz nadać rolę.");
    }

    public RoleDTO showRole(String username) {

        Optional<Role> result = roleRepository.findByUsername(username);
        if (result.isPresent()) {
            Role role = result.get();
            return RoleDTO.builder().role(role.getRole()).
                    username(role.getUsername()).build();
        }
        throw new InvalidRoleDataException("Nie istnieje konto o podanym loginie (username). ");
    }

    public void deleteRole(String username) {

        Optional<Role> result = roleRepository.findByUsername(username);
        if (result.isPresent()) {
            Role role = result.get();
            role.setRole("");
            roleRepository.save(role);
        } else throw new InvalidRoleDataException("Użytkownik nie posiada przypisanej roli.");
    }
}
