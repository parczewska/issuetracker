package pl.pa.issue.tracker.role.api;

import org.springframework.web.bind.annotation.*;
import pl.pa.issue.tracker.role.domain.RoleService;
import pl.pa.issue.tracker.role.model.RoleDTO;

@RestController
public class RoleApi {

    private RoleService roleService;

    public RoleApi(RoleService roleService) {

        this.roleService = roleService;
    }

    @RequestMapping(value = "/roles/{username}", method = RequestMethod.GET)
    public RoleDTO show(@PathVariable String username) {
        return roleService.showRole(username);
    }

    @RequestMapping(value = "/roles/{username}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String username) {
        roleService.deleteRole(username);
    }

    @RequestMapping(value = "/roles", method = RequestMethod.POST)
    public RoleDTO assign(@RequestBody RoleDTO roleDTO) {
        return roleService.assignRole(roleDTO);
    }
}
