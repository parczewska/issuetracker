package pl.pa.issue.tracker.role.model;

import javax.persistence.*;

@Entity
@Table(name = "available_roles")

public class AvailableRole {

    @Id
    @Column(name = "role")
    private String role;

    public AvailableRole() {
    }

    public AvailableRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
