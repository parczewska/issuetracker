package pl.pa.issue.tracker.role.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "roles")

public class Role {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;
    @Column(name = "role")
    private String role;
    @Column(name = "username")
    private String username;


    public String getRole() {
        return role;
    }

    public String getUsername() {
        return username;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
