package pl.pa.issue.tracker.role.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class RoleDTO {

    @JsonProperty
    private String role;

    @JsonProperty
    private String username;

    public String getRole() {
        return role;
    }

    public String getUsername() {
        return username;
    }
}
